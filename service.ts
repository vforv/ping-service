import { PingActions } from './actions/ping.actions';
import * as Hemera from 'nats-hemera';
const HemeraStats = require('hemera-stats');

const nats = require('nats').connect({
  'url': `nats://${process.env.TITAN_NATS_SERVICE_HOST}:4222`,
  'user': process.env.NATS_CLUSTER_USER,
  'pass': process.env.NATS_CLUSTER_PASSWORD
})

const hemera = new Hemera(nats, { logLevel: 'info' })

hemera.use(HemeraStats);

hemera.ready(() => {

  const ping = new PingActions(hemera)
  ping.getPingAction();

});