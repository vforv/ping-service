FROM node:6.9.4-alpine

# Create app directory
RUN mkdir -p /home/app
RUN mkdir -p /home/app/test
RUN mkdir -p /home/app/profile-service
WORKDIR /home/app


# Install app dependencies

COPY package.json /home/app/profile-service
RUN cd profile-service && npm install;


# Bundle app source
COPY . /home/app/profile-service

CMD cd profile-service ; npm start