##Ping service

#Main maintainer

Vladimir Djukic

##Description:

This service communicate with api and use just for testing.

##Messages

| Feature  | Message |
| ------------- | ------------- |
| Login user  | `topic: 'ping',cmd: 'date'`  |

- Message params:

        format: formated|timestamp;

- Message response:

        date: October 4th 2017, 1:54:33 pm

- Message description:

        This message just use for testing api you can call it from api route host:port/v1/ping/formated

##Env variables

    NATS_URL: nats://localhost:4222
    NATS_USER: ruser
    NATS_PW: T0pS3cr3t